﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Repeller : MonoBehaviour
{
    public float force = 50;
    public float damping = 50;
    [Range(0f, 1f)]
    public float innerRadiusMultiplier = 0.8f;

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.GetComponent<Rigidbody2D>() == null)
            return;

        Vector2 deltaPos = other.transform.position - transform.position;
        var radiusVecNormalized = deltaPos / GetComponent<CircleCollider2D>().radius;
        var forceFactor = Mathf.InverseLerp(innerRadiusMultiplier, 1, radiusVecNormalized.magnitude);

        var relativeVelocity = other.GetComponent<Rigidbody2D>().velocity - GetComponent<Rigidbody2D>().velocity;
        var dampingVec = -relativeVelocity * damping;

        other.GetComponent<Rigidbody2D>().AddForce(radiusVecNormalized * force * forceFactor * forceFactor + dampingVec);
    }

}
