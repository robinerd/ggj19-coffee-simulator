﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoffeeSource : MonoBehaviour
{
    public float spawnInterval = 0.2f;
    public GameObject coffeePrefab;

    float timer = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;

        if(Input.GetButton("Fire1") && timer < 0)
        {
            Instantiate(coffeePrefab, transform.position, Quaternion.identity);
            timer = spawnInterval;
        }
    }
}
