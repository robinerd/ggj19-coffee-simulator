﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateWithButton : MonoBehaviour
{
    public string buttonName = "Fire1";
    public float addAngle = 50;
    public float linearSpeed = 10;

    float originalAngle;
    private float timePressed;

    void Start()
    {
        originalAngle = transform.localRotation.eulerAngles.z;
    }

    void Update()
    {
        float targetAngle = originalAngle;
        if (Input.GetButton(buttonName))
        {
            targetAngle += addAngle;
            timePressed += Time.deltaTime;
        }
        else
            timePressed = 0;

        //float speedBoostFactor = 1 + 10 * Mathf.Clamp01(timePressed / 2.0f);
        float newAngle = Mathf.MoveTowardsAngle(transform.localRotation.eulerAngles.z, targetAngle, linearSpeed * Time.deltaTime);
        transform.localRotation = Quaternion.Euler(0, 0, newAngle);
    }
}
