﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public Text scoreText;
    int score;

    public static ScoreManager instance;

    private void Awake()
    {
        instance = this;
    }

    public void ResetScore()
    {
        score = 0;
        scoreText.text = "0";
    }

    public void AddScore(int addedScore)
    {
        if (!RoundManager.instance.isPlaying)
            return;

        score += addedScore;
        scoreText.text = score.ToString();
    }

    public int GetScore()
    {
        return score;
    }
}