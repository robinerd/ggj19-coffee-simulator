﻿using UnityEngine;
using UnityEngine.UI;

public class RoundManager : MonoBehaviour
{
    public Text timeText;
    public Image timeImage;
    public float timeLimit = 30;
    public GameObject gameOverGUI;
    public GameObject restartButton;

    public static RoundManager instance;

    private bool _isPlaying = false;
    public bool isPlaying {
        get
        {
            return _isPlaying;
        }
        private set
        {
            _isPlaying = value;
            restartButton.SetActive(value);
        }
    }

    float _timeLeft;
    float timeLeft
    {
        get
        {
            return _timeLeft;
        }
        set
        {
            _timeLeft = value;
            timeText.text = value > 0 ? value.ToString("0") : "Time's Up!";
            timeImage.fillAmount = Mathf.Clamp01(value / timeLimit);
        }
    }

    private void Awake()
    {
        instance = this;
    }

    public void NewRound()
    {
        timeLeft = timeLimit;
        gameOverGUI.SetActive(false);
        CoffeePot.instance.ClearCoffee();
        isPlaying = true;
    }

    void GameOver()
    {
        isPlaying = false;
        gameOverGUI.SetActive(true);
    }

    private void Update()
    {
        if (isPlaying)
        {
            timeLeft -= Time.deltaTime;
            if(timeLeft <= 0)
            {
                timeLeft = 0;
                GameOver();
            }
        }
        //else if (waitingForKeypress)
        //{
        //    if(Input.GetButtonDown("Shoulder") ||
        //        Input.GetButtonDown("Elbow") ||
        //        Input.GetButtonDown("Head") ||
        //        Input.GetButtonDown("Coffee"))
        //    {
        //        waitingForKeypress = false;
        //        NewRound();
        //    }
        //}
    }
}