﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoffeePot : MonoBehaviour
{
    public string buttonName = "Coffee";

    public static CoffeePot instance;

    private void Awake()
    {
        instance = this;
    }

    private void Update()
    {
        GetComponent<Water2D.Water2D_Spawner>().enabled = Input.GetButton(buttonName);
    }

    public void ClearCoffee()
    {
        Water2D.Water2D_Spawner.instance.Restore();

        Water2D.Water2D_Spawner.instance.Dynamic = true;
        Water2D.Water2D_Spawner.instance.Spawn();
    }

}
