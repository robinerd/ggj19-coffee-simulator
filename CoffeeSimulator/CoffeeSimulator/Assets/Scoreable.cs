﻿using UnityEngine;

public class Scoreable : MonoBehaviour
{
    [HideInInspector]
    public bool scored = false;
}