﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreTrigger : MonoBehaviour
{
    public int score;

    private void OnTriggerEnter2D(Collider2D other)
    {
        Scoreable scoreable = other.GetComponent<Scoreable>();
        if(scoreable != null)
        {
            scoreable.scored = true;
            ScoreManager.instance.AddScore(score);
        }
    }
}
