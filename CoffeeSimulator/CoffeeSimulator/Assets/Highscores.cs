﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class Highscores : MonoBehaviour
{
    public int maxEntries = 5;
    public Text names;
    public Text scores;

    public static Highscores instance;

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    private void OnEnable()
    {
        LoadScores();
    }

    struct ScoreEntry
    {
        public string name;
        public int score;
    }

    public void AddScoreIfBetter(string name, int score)
    {
        var scores = getCurrentScores();

        scores.Add(new ScoreEntry
        {
            name = name,
            score = score
        });

        scores.Sort((row1, row2) => row2.score - row1.score);

        int i = 1;
        foreach (var entry in scores.Take(maxEntries))
        {
            PlayerPrefs.SetString("HighscoreName_" + i, entry.name);
            PlayerPrefs.SetInt("HighscoreScore_" + i, entry.score);
            i++;
        }

        LoadScores();
    }

    // Update is called once per frame
    public void LoadScores()
    {
        string names = "";
        string scores = "";
        foreach(var scoreEntry in getCurrentScores())
        {
            names += scoreEntry.name + "\n";
            scores += scoreEntry.score + "\n";
        }
        this.names.text = names;
        this.scores.text = scores;
    }

    List<ScoreEntry> getCurrentScores()
    {

        List<ScoreEntry> scores = new List<ScoreEntry>();
        for (int i = 1; i <= maxEntries; i++)
        {
            var scoreName = PlayerPrefs.GetString("HighscoreName_" + i, "");
            if (scoreName == "")
                break;

            scores.Add(new ScoreEntry()
            {
                name = scoreName,
                score = PlayerPrefs.GetInt("HighscoreScore_" + i, 0)
            });
        }
        return scores;
    }

    private void Update()
    {
        //Hotkey to clear scores.
        if (Input.GetKeyDown(KeyCode.Backspace) && Input.GetKey(KeyCode.LeftShift))
        {
            PlayerPrefs.DeleteAll();
            LoadScores();
        }
    }
}
