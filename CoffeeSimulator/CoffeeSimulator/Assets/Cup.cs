﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Water2D;

public class Cup : MonoBehaviour
{
    [Range(0,1)]
    public float inheritVelocity = 0.8f;
    public float minTiltAngle;
    public float maxTiltAngle;
    public float delayAtMinTilt;
    public float delayAtMaxTilt;
    public Vector3 localVelocity;
    public Vector3 globalVelocity;

    Water2D_Spawner spawner;
    Vector2 prevPosition;
    Vector2 velocity = Vector2.zero;

    // Start is called before the first frame update
    void Start()
    {
        prevPosition = transform.position;
        spawner = GetComponent<Water2D.Water2D_Spawner>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Time.fixedDeltaTime == 0) //Shouldn't happen but better to check and avoid division by zero.
            return;

        Vector2 currentVelocity = ((Vector2)transform.position - prevPosition) / Time.fixedDeltaTime;
        velocity = 0.5f * velocity + 0.5f * currentVelocity;

        prevPosition = transform.position;

        float tiltAmount;
        if (transform.eulerAngles.z < 0 || transform.eulerAngles.z > 180)
            tiltAmount = 0;
        else
            tiltAmount = Mathf.InverseLerp(minTiltAngle, maxTiltAngle, transform.eulerAngles.z);

        if(tiltAmount <= 0)
        {
            spawner.DelayBetweenParticles = 0.8f;
        }
        else
        {
            spawner.DelayBetweenParticles = Mathf.Lerp(delayAtMinTilt, delayAtMaxTilt, tiltAmount);
        }

        Vector2 startVelocity = transform.TransformVector(localVelocity) + globalVelocity;
        spawner.initSpeed = velocity * inheritVelocity + tiltAmount * startVelocity;
    }
}
