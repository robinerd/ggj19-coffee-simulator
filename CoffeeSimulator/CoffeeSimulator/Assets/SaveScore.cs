﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaveScore : MonoBehaviour
{
    public InputField nameInput;

    private void OnEnable()
    {
        nameInput.interactable = true;
        GetComponentInChildren<Text>().text = "Save Score";
        GetComponent<Button>().interactable = true;
    }

    public void SaveIfBetter()
    {
        string name = nameInput.text;
        if (name == "")
            return;

        Highscores.instance.AddScoreIfBetter(name, ScoreManager.instance.GetScore());

        nameInput.interactable = false;
        GetComponentInChildren<Text>().text = "Thanks!";
        GetComponent<Button>().interactable = false;
    }
}
